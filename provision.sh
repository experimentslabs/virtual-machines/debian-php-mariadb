#!/bin/sh

set -xe

export DEBIAN_FRONTEND=noninteractive

# Node repo
curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash -
# Yarn repo
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/yarnkey.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
# Symfony repo
echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | sudo tee /etc/apt/sources.list.d/symfony-cli.list

apt-get update
apt-get install --assume-yes \
    curl git build-essential \
    vim git htop tree lnav
    php php-gd php-curl php-zip php-xml php-dom php-imap php-mbstring\
    nodejs yarn \
    mariadb-server mariadb-client \
    symfony-cli

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '906a84df04cea2aa72f40b5f787e49f22d4c2f19492ac310e8cba5b96ac8b64115ac402c8cd292b8a03482574915d1a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
sudo mv composer.phar /usr/local/bin/composer
php -r "unlink('composer-setup.php');"
